/**
  ******************************************************************************
  * File Name          : epos4_serial.h
  * Description        : Contains functions that
  * simplify the configuration and communication with the EPOS4 positioning controller.
  *
  * It is referred to the EPOS4 communication guide and firmware spec. datasheets.
  * 
  * Author: Nikolas Schroeder (lrt86824@stud.uni-stuttgart.de)
  ******************************************************************************
**/
#include "stm32f7xx_hal.h"

/***************************************************/
/* DEFINITIONS  */
/***************************************************/


//Device Control

#define SHUTDOWN 0x06
#define SWITCH_ON 0x07
#define SWITCH_ON_ENABLE_OP 0x0F
#define DISABLE_VOLT 0x00
#define QUICK_STOP 0x02
#define DISABLE_OP 0x08
#define ENABLE_OP 0x09
#define FAULT_RESET 0x80

//Macro functions
#define ROUND_2_INT16(f) ((int16_t)(f >= 0.0 ? (f + 0.5) : (f - 0.5)))
#define ROUND_2_INT32(f) ((int32_t)(f >= 0.0 ? (f + 0.5) : (f - 0.5)))

#define PI (double)3.14159

//protocol related
#define NODE_ID 0x01 //Node ID of EPOS controller, change here if not 0x01
#define DLE 0x90 // Starting frame character, Data Link Escape
#define STX 0x02 // Starting frame character, Start of Text

//Operation Mode
#define EPOS_PPM 0x01 // Profile Position Mode (not yet considered in the scope of this driver)
#define EPOS_PVM 0x03 // Profile Velocity Mode (not yet considered in the scope of this driver)
#define EPOS_HMM 0x06 // Homing Mode (not yet considered in the scope of this driver)
#define EPOS_CST 0x0A // Cyclic Synchronous Torque Mode
#define EPOS_CSV 0x09 // Cyclic Synchronous Velocity Mode
#define EPOS_CSP 0x08 // Cyclic Synchronous Position Mode



//motor types
#define DC_MOTOR 0x01 //Brushed DC motor
#define EC_SINUS 0x0A //Brushless DC motor sinus commutated
#define EC_BLOCK 0x0B ////Brushless DC motor block commutated

//SI Units
#define DIMLESS 0x00 //dimensionless
#define METER 0x01 //meter
#define KILOG 0x02 //kilogramm
#define SECS 0x03 //Second
#define AMPERE 0x04 //Ampere
#define MINS 0x47 //Minute
#define SQSEC 0x57 //Square Second

//Application profile specific units
#define REVS 0xB4 //revolutions
#define INCS 0xB5 //increments
#define STPS 0xAC //Steps

//Units Prefixes
#define MEGA 0x06
#define KILO 0x03
#define HECTA 0x02
#define DECA 0x01
#define NOPREFIX 0x00
#define DECI 0xFF
#define CENTI 0xFE
#define MILLI 0xFD
#define MICRO 0xFA

//polarity of hall sensor, direction of encoder
#define P_MAXON 0x00
#define P_INVERSE 0x01

/***************************************************/
/* Typedefs  */
/***************************************************/


//TODO Somewhere define the motor rated torque as const. value
//	   Defined as nominal current times torque constant

//MotorData struct
typedef struct MData
{
	uint32_t nomCurrent; //nominal current in [mA]
    uint32_t limCurrent; //output current limit
    uint8_t numPolePairs; //number of pole pairs
    uint16_t timeConst; //thermal time constant winding
    uint32_t torqueConst; //torque constant
    uint32_t maxSpeed; //max motor speed (as given in datasheet)
}MData;

//Axis configuration Struct
typedef struct MAxis
{
	uint32_t sensor; //sensor configuration
    uint32_t control; //control structure configuration
    uint32_t commutation; //Defines the control structure of the axis dependent on the available sensors and their disposition.
    uint32_t micsell; //Used to define various options regarding the axis configuration
}MAxis;

//Motor Units Struct
typedef struct MUnits
{
	uint8_t accel_num; //Angular acceleration unit numerator
	uint8_t accel_den; //Angular acceleration unit denominator
	uint8_t accel_pre; //Angular acceleration unit prefix
	uint8_t velo_num; //Angular velocity unit numerator
	uint8_t velo_den; //Angular velocity unit denominator
	uint8_t velo_pre; //Angular velocity unit prefix
	uint8_t pos_num; //position unit numerator
	uint8_t pos_den; //position unit denominator
	uint8_t pos_pre; //position unit prefix
}MUnits;

//Digital incremental Encoder Struct
typedef struct MEncoder
{
	uint32_t pulses_per_rev; //pulses per revolution
	uint8_t direction; // maxon (=0x00); inverted (=0x01) No other values permitted!
	uint8_t ind; //2-channel (=0x00); 3-channel (=0x01) NO other values permitted!
}MEncoder;

//Current Control Gains struct
typedef struct CurrCtrl
{
	uint32_t P_gain; // proportional gain of the current controller
	uint32_t I_gain; // integral gain of the current controller
}CurrCtrl;

//Position Control Gains Struct
typedef struct PosCtrl
{
	uint32_t P_gain; // proportional gain of the position controller
	uint32_t I_gain; // integral gain of the position controller
	uint32_t D_gain; // derivative gain of the position controller
	uint32_t FF_velo_gain; // speed feed forward gain of the position controller
	uint32_t FF_accel_gain; // acceleration feed forward gain of the position controller
}PosCtrl;

//Velocity Control Gains Struct
typedef struct VeloCtrl
{
	uint32_t P_gain; // proportional gain of the velocity controller
	uint32_t I_gain; // integral gain of the velocity controller
	uint32_t FF_velo_gain; // speed feed forward gain of the velocity controller
	uint32_t FF_accel_gain; // acceleration feed forward gain of the velocity controller
}VeloCtrl;

//

//Define Motor Meta Struct
typedef struct EPOSMeta
{
	uint16_t type;
	MData data;
	MAxis axis;
	MEncoder enc;
	uint8_t hall_polarity;
	MUnits units; //Due to the implementation of some functions it is recommended to set the position unit to [increments] and the velocity unit to [increments/s]
    int8_t op_mode;
    CurrCtrl curr_gains; //Thus far, not taken into account in epos_Config()
    PosCtrl pos_gains; //Thus far, not taken into account in epos_Config()
    VeloCtrl velo_gains; //Thus far, not taken into account in epos_Config()
    uint32_t rated_torque;
    double min_pos_lim; // if both min. and max. pos_limit are set to zero, the limit check is disabled!
    double max_pos_lim;
    double torque_offset; //[Nm] Definition only needed when operating in Cyclic Synchronous Torque Mode or Cyclic Synchronous Position mode
    double velo_offset; //[rad/s] Definition only needed when operating in Cyclic Synchronous Velocity mode
    double pos_offset; //[rad] Definition only needed when operating in Cyclic Synchronous Position mode
    double divisor;
}EPOSMeta;

/***************************************************/
/* Function Declarations */
/***************************************************/

//Get the 16-bit CRC checksum using the algorithm CRC-CCIT (the function is taken from the EPOS Communication Guide p. 12)
uint16_t epos_CalcFieldCRC(uint16_t *pDataArray, uint16_t numberOfWords);


//basic read and write access

HAL_StatusTypeDef epos_ReadObject(UART_HandleTypeDef *handle, uint16_t index, uint8_t subindex, uint32_t *databuffer);
HAL_StatusTypeDef epos_WriteObject(UART_HandleTypeDef *handle, uint16_t index, uint8_t subindex, uint8_t *databuffer, uint8_t datasize);
//Config related
HAL_StatusTypeDef epos_SetMotorType(UART_HandleTypeDef *handle, uint16_t type);
HAL_StatusTypeDef epos_SetMotorData(UART_HandleTypeDef *handle, MData motor);
HAL_StatusTypeDef epos_GetMotorRatedTorque(UART_HandleTypeDef *handle, uint32_t *rated_torque);
HAL_StatusTypeDef epos_SetMotorAxis(UART_HandleTypeDef *handle, MAxis axis);
HAL_StatusTypeDef epos_ConfigEncoder(UART_HandleTypeDef *handle, MEncoder enc);
HAL_StatusTypeDef epos_SetHallPolarity(UART_HandleTypeDef *handle, uint8_t polarity);
HAL_StatusTypeDef epos_SetUnits(UART_HandleTypeDef *handle, MUnits units);
HAL_StatusTypeDef epos_SetOperationMode(UART_HandleTypeDef *handle, int8_t mode);
HAL_StatusTypeDef epos_SetMaxSpeed(UART_HandleTypeDef *handle, MData motor, uint16_t type);
HAL_StatusTypeDef epos_SetCurCtrlGains(UART_HandleTypeDef *handle, CurrCtrl gains);
HAL_StatusTypeDef epos_SetPosCtrlGains(UART_HandleTypeDef *handle, PosCtrl gains);
HAL_StatusTypeDef epos_SetVeloCtrlGains(UART_HandleTypeDef *handle, VeloCtrl gains);
HAL_StatusTypeDef epos_SetTorqueOffset(UART_HandleTypeDef *handle, double offset, double divisor);
HAL_StatusTypeDef epos_SetPosOffset(UART_HandleTypeDef *handle, double offset, double numIncr);
HAL_StatusTypeDef epos_SetVeloOffset(UART_HandleTypeDef *handle, double offset, double numIncr);
HAL_StatusTypeDef epos_SetPosLimit(UART_HandleTypeDef *handle, double min, double max, double numIncr);
HAL_StatusTypeDef epos_StoreParameters(UART_HandleTypeDef *handle);
HAL_StatusTypeDef epos_RestoreParameters(UART_HandleTypeDef *handle);
HAL_StatusTypeDef epos_GetStatusword(UART_HandleTypeDef *handle, uint16_t *statusword);
HAL_StatusTypeDef epos_DeviceCommand(UART_HandleTypeDef *handle, uint8_t command);

HAL_StatusTypeDef epos_SetDesiredTorque(UART_HandleTypeDef *handle, double torque, double divisor);
HAL_StatusTypeDef epos_SetDesiredPos(UART_HandleTypeDef *handle, double pos, double numIncr);
HAL_StatusTypeDef epos_SetDesiredVelo(UART_HandleTypeDef *handle, double velo, double numIncr);
HAL_StatusTypeDef epos_GetActualTorque(UART_HandleTypeDef *handle, double *torque, double divisor);
HAL_StatusTypeDef epos_GetActualPos(UART_HandleTypeDef *handle, double *pos, double numIncr);
HAL_StatusTypeDef epos_GetActualSpeed(UART_HandleTypeDef *handle, double *speed, double numIncr);
HAL_StatusTypeDef epos_Config(UART_HandleTypeDef *handle, EPOSMeta *meta);



















