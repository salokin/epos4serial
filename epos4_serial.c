/**
  ******************************************************************************
  * File Name          : epos4_serial.c
  * Description        : Contains functions that
  * simplify the configuration and communication with the EPOS4 positioning controller.
  *
  * It is referred to the EPOS4 communication guide and firmware spec. datasheets.
  * 
  * Author: Nikolas Schroeder (lrt86824@stud.uni-stuttgart.de)
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
#include "epos4_serial.h"

/* Get the 16-bit CRC checksum using the algorithm CRC-CCIT
 * (the function is taken from the EPOS Communication Guide p. 12)
 *
 *
 * Important:
 * The input argument is an array consisting of words (uint16_t).
 * This input array has to include header and data of the request frame
 * (cmp. p. 11 of the EPOS4 Communication Guide). Since header and
 * data consist of multiple bytes (uint8_t) and not words, the bytes
 * have to be connected in the right way!
 *
 * Example: If we have a sequence of two single bytes, then the first
 * byte is the least significant and the second byte is the most significant.
 * This means if those two bytes shall be combined in a word, the order
 * has to be switched!
 *
 * uint8_t A = 0xff, uint8_t B = 0x11, sequence: A -> B  => uint16_t BA = 0x11ff !!
 *
 * Nice to know: If a word (uint16_t) shall be transmitted over the serial interface
 * as two single bytes, the example has to be reversed. In the sequence, the least
 * significant byte is transmitted first and the most significant byte second.
 *
 * uint16_t BA = 0x11ff => sequence: 0xff -> 0x11 !!
 *
 *
 * Complete example on the usage of CalcFieldCRC():
 *
 * Object Read Get Velocity Units:
 * Header.OpCode = 0x60
 * Header.Length = 0x02
 * Data.NodeID = 0x01
 * Data.Index = 0x60A9
 * Data.Subindex = 0x00
 *
 * This results in the following sequence of single bytes:
 * 0x60 -> 0x02 -> 0x01 -> 0xA9 -> 0x60 -> 0x00
 *
 * The sequence results in the four words as they can be seen
 * in data_array which is the input to the function. The last word
 * is the zero word and always necessary. Here, the input
 * argument numberOfWords is the number of array elements.
 *
 * uint16_t crc_result = 0;
 * uint16_t data_array[4];
 *
 * data_array[0] = 0x0260;
 * data_array[1] = 0xA901;
 * data_array[2] = 0x0060;
 * data_array[3] = 0x0000;
 *
 * crc_result = CalcFieldCRC(data_array, 4);
 *
*/
uint16_t epos_CalcFieldCRC(uint16_t *pDataArray, uint16_t numberOfWords){
	uint16_t shifter, c;
	uint16_t carry;
	uint16_t result = 0x0000;

	while(numberOfWords--){
		shifter = 0x8000;
		c = *pDataArray++;
		do{
			carry = result & 0x8000;
			result <<= 1;
			if(c & shifter) result++;
			if(carry) result ^= 0x1021;
			shifter >>=1;
		}while(shifter);
	}
	return result;
}

//basic read and write access functions
//In the EPOS nomenclature registers are referred to as objects.

/* Basic read object function as it is described in the EPOS4 Communication Guide
 * on p. 7
 * */
HAL_StatusTypeDef epos_ReadObject(UART_HandleTypeDef *handle, uint16_t index, uint8_t subindex, uint32_t *databuffer){

	HAL_StatusTypeDef status;
	uint8_t request_frame[10]; //the request frame does not consider character stuffing because there are no bytes with the value 0x90
	uint8_t response_frame[14];
	uint16_t crc_array[4];
	uint16_t crc_result;

	//prepare request frame
	request_frame[0] = DLE; //SYNC
	request_frame[1] = STX; //SYNC
	request_frame[2] = 0x60; //HEADER OP code
	request_frame[3] = 0x02; //HEADER Length
	request_frame[4] = NODE_ID; //Data.NodeID
	request_frame[5] = (uint8_t)(0x00ff & index); //Data.index Least significant byte
	request_frame[6] = (uint8_t)(index>>8); //Data.index Most significant byte
	request_frame[7] = subindex; //Data.subindex

    crc_array[0] = (((uint16_t)request_frame[3])<<8) | ((uint16_t)request_frame[2]);
    crc_array[1] = (((uint16_t)request_frame[5])<<8) | ((uint16_t)request_frame[4]);
    crc_array[2] = (((uint16_t)request_frame[7])<<8) | ((uint16_t)request_frame[6]);
    crc_array[3] = 0x0000;

    crc_result = epos_CalcFieldCRC(crc_array, 4);

	request_frame[8] = (uint8_t)(0x00ff & crc_result); //crc_result: Least significant byte
	request_frame[9] = (uint8_t)(crc_result>>8); //crc_result: Most significant byte

	//Transmit request frame
    status = HAL_UART_Transmit(handle, request_frame, 10, HAL_MAX_DELAY);

	//Receive response frame and handle possible character stuffing
    status = HAL_UART_Receive(handle, response_frame, 8, HAL_MAX_DELAY); //receive the first eight bytes (DLE,STX;OP;LEN;LowErrorWord,HighErrorWord)
    //TODO error handling by processing response_frame[4]/[5]/[6]/[7]
	
    status = HAL_UART_Receive(handle, &response_frame[8], 1, HAL_MAX_DELAY); //receive first actual data byte

    for(int i = 8; i<11; i++){
    	if(response_frame[i] == 0x90){ //check if its value is 0x90, if yes it is a matter of fact that a 2nd 0x90 will be following because of character stuffing
    	    	status = HAL_UART_Receive(handle, &response_frame[i+1], 2, HAL_MAX_DELAY);
    	    	response_frame[i+1] = response_frame[i+2];
    	}
    	else status = HAL_UART_Receive(handle, &response_frame[i+1], 1, HAL_MAX_DELAY);
    }

    if(response_frame[11] == 0x90) status = HAL_UART_Receive(handle, &response_frame[12], 1, HAL_MAX_DELAY);

    //receive CRC
    status = HAL_UART_Receive(handle, &response_frame[12], 2, HAL_MAX_DELAY);


    //Process response frame
	//Extract data from received response frame
    *databuffer = ((uint32_t)response_frame[8]) | (((uint32_t)response_frame[9])<<8) | (((uint32_t)response_frame[10])<<16) | (((uint32_t)response_frame[11])<<24);

    //TODO Calculate CRC from response frame and compare with received CRC. If not matching -> Error

	return status;
}


/* Basic write object function as it is described in the EPOS4 Communication Guide
 * on p. 8
 *
 * */
HAL_StatusTypeDef epos_WriteObject(UART_HandleTypeDef *handle, uint16_t index, uint8_t subindex, uint8_t *databuffer, uint8_t datasize){

	HAL_StatusTypeDef status;
	uint8_t request_frame[14];
	uint8_t response_frame[10];
	uint16_t crc_array[6];
	uint16_t crc_result;

	//prepare request frame
	request_frame[0] = DLE; //SYNC
	request_frame[1] = STX; //SYNC
	request_frame[2] = 0x68; //HEADER OP code
	request_frame[3] = 0x04; //HEADER Length
	request_frame[4] = NODE_ID; //Data.NodeID
	request_frame[5] = (uint8_t)(0x00ff & index); //Data.index Least significant byte
	request_frame[6] = (uint8_t)(index>>8); //Data.index Most significant byte
	request_frame[7] = subindex; //Data.subindex

	request_frame[8] = 0x00;
	request_frame[9] = 0x00;
	request_frame[10] = 0x00;
	request_frame[11] = 0x00;


	for(int i = 0; i<datasize; i++){
		request_frame[i+8] = *databuffer;
		databuffer++;
	}

	crc_array[0] = (((uint16_t)request_frame[3])<<8) | ((uint16_t)request_frame[2]);
	crc_array[1] = (((uint16_t)request_frame[5])<<8) | ((uint16_t)request_frame[4]);
	crc_array[2] = (((uint16_t)request_frame[7])<<8) | ((uint16_t)request_frame[6]);
	crc_array[3] = (((uint16_t)request_frame[9])<<8) | ((uint16_t)request_frame[8]);
	crc_array[4] = (((uint16_t)request_frame[11])<<8) | ((uint16_t)request_frame[10]);
	crc_array[5] = 0x0000;

	crc_result = epos_CalcFieldCRC(crc_array, 6);

	request_frame[12] = (uint8_t)(0x00ff & crc_result); //crc_result: Least significant byte
	request_frame[13] = (uint8_t)(crc_result>>8); //crc_result: Most significant byte

	/* Character Stuffing - Check EPOS4 Communication Guide on p. 13
	 * It is assumed that there are no object indices or node IDs that contain the value 0x90.
	 * Hence, solely the two actual data words (4 bytes) are considered for character stuffing.
	 */
	int stuffing_length = 0;
	for(int i = 8; i<12; i++){
		if(request_frame[i] == 0x90) stuffing_length ++;
	}

	if(stuffing_length == 0){
		//Transmit original request frame
		status = HAL_UART_Transmit(handle, request_frame, 14, HAL_MAX_DELAY);
	}
	else{
		uint8_t request_frame_stuffed[14 + stuffing_length];
		int i = 0; //iterator
		int u = 0; //stuffed iterator

		while(i < 8){
				request_frame_stuffed[u] = request_frame[i];
				i++;
				u++;
			}
		while(i < 12){
				if(request_frame[i] == 0x90){
					request_frame_stuffed[u] = request_frame[i];
					request_frame_stuffed[u+1] = request_frame[i];
					i++;
					u+=2;
				}
				else{
					request_frame_stuffed[u] = request_frame[i];
					i++;
					u++;
				}
		request_frame_stuffed[u] = request_frame[i];
		request_frame_stuffed[u+1] = request_frame[i+1];
		}
		status = HAL_UART_Transmit(handle, request_frame_stuffed, (14+stuffing_length), HAL_MAX_DELAY);
	}

	/* The receive frame after writing to an object does not contain stuffed bytes and therefore does
	 * not require an additional decoding. This is because the data that is contained in the response frame
	 * keeps the communication error code. The error code does not contain a byte with the value 0x90. (compare
	 * epos4 communication guide p. 37)
     */

	//Receive response frame
	status = HAL_UART_Receive(handle, response_frame, 10, HAL_MAX_DELAY);

	//TODO Extract Communication Error Code
	//TODO Calculate CRC from response frame and compare with received CRC. If not matching -> Error

	return status;
}


/* Convenience Functions
*/

//CONFIG related

HAL_StatusTypeDef epos_SetMotorType(UART_HandleTypeDef *handle, uint16_t type){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	databuffer[0] = (uint8_t)(0x00ff & type);
	databuffer[1] = (uint8_t)(type>>8);
	status = epos_WriteObject(handle, 0x6402, 0x00, databuffer, 2);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_SetUnits(UART_HandleTypeDef *handle, MUnits units){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	//set ACC unit
	databuffer[1] = units.accel_den;
	databuffer[2] = units.accel_num;
	databuffer[3] = units.accel_pre;
	status =epos_WriteObject(handle, 0x60AA, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	//set VELO unit
	databuffer[1] = units.velo_den;
	databuffer[2] = units.velo_num;
	databuffer[3] = units.velo_pre;
	status = epos_WriteObject(handle, 0x60A9, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	//set POS unit
	databuffer[1] = units.pos_den;
	databuffer[2] = units.pos_num;
	databuffer[3] = units.pos_pre;
	status = epos_WriteObject(handle, 0x60A8, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_SetMotorData(UART_HandleTypeDef *handle, MData motor){
	HAL_StatusTypeDef status;
    uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

    //nominal current
    databuffer[0] = (uint8_t)(0x000000ff & motor.nomCurrent);
    databuffer[1] = (uint8_t)(0x000000ff & (motor.nomCurrent>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (motor.nomCurrent>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (motor.nomCurrent>>24));
	status = epos_WriteObject(handle, 0x3001, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

	/*Max output current: It is recommended to set the max output current to the double of the nominal current (EPOS firmware spec. p. 134)*/
	//output current limit
	databuffer[0] = (uint8_t)(0x000000ff & motor.limCurrent);
	databuffer[1] = (uint8_t)(0x000000ff & (motor.limCurrent>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (motor.limCurrent>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (motor.limCurrent>>24));
	status = epos_WriteObject(handle, 0x3001, 0x02, databuffer, 4);
	if(status != HAL_OK) return status;

	//number of pole pairs
	databuffer[0] = motor.numPolePairs;
	status = epos_WriteObject(handle, 0x3001, 0x03, databuffer, 1);
	if(status != HAL_OK) return status;

	//thermal time constant winding
	databuffer[0] = (uint8_t)(0x00ff & motor.timeConst);
	databuffer[1] = (uint8_t)(motor.nomCurrent>>8);
	status = epos_WriteObject(handle, 0x3001, 0x04, databuffer, 2);
	if(status != HAL_OK) return status;

	//torque constant
	databuffer[0] = (uint8_t)(0x000000ff & motor.torqueConst);
	databuffer[1] = (uint8_t)(0x000000ff & (motor.torqueConst>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (motor.torqueConst>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (motor.torqueConst>>24));
	status = epos_WriteObject(handle, 0x3001, 0x05, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

/*
 * This function reads out the motor rated torque which is stored in (0x6076) and has the unit [uNm]
 * It equals the product of of the motors nominal current and torque constant
 */
HAL_StatusTypeDef epos_GetMotorRatedTorque(UART_HandleTypeDef *handle, uint32_t *rated_torque){
	HAL_StatusTypeDef status;

    status = epos_ReadObject(handle, 0x6076, 0x00, rated_torque);
    if(status != HAL_OK) return status;

	return status;
	}


HAL_StatusTypeDef epos_SetMotorAxis(UART_HandleTypeDef *handle, MAxis axis){
	HAL_StatusTypeDef status;
    uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

    //sensor types used for the axis
    databuffer[0] = (uint8_t)(0x000000ff & axis.sensor);
    databuffer[1] = (uint8_t)(0x000000ff & (axis.sensor>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (axis.sensor>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (axis.sensor>>24));
	status = epos_WriteObject(handle, 0x3000, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

    //control structure
    databuffer[0] = (uint8_t)(0x000000ff & axis.control);
    databuffer[1] = (uint8_t)(0x000000ff & (axis.control>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (axis.control>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (axis.control>>24));
	status = epos_WriteObject(handle, 0x3000, 0x02, databuffer, 4);
	if(status != HAL_OK) return status;

    //defines the commutation sensors for the axis motor
    databuffer[0] = (uint8_t)(0x000000ff & axis.commutation);
    databuffer[1] = (uint8_t)(0x000000ff & (axis.commutation>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (axis.commutation>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (axis.commutation>>24));
	status = epos_WriteObject(handle, 0x3000, 0x03, databuffer, 4);
	if(status != HAL_OK) return status;

    //Used to define various options regarding the axis configuration.
    databuffer[0] = (uint8_t)(0x000000ff & axis.micsell);
    databuffer[1] = (uint8_t)(0x000000ff & (axis.micsell>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (axis.micsell>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (axis.micsell>>24));
	status = epos_WriteObject(handle, 0x3000, 0x04, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef  epos_ConfigEncoder(UART_HandleTypeDef *handle, MEncoder enc){
	HAL_StatusTypeDef status;
    uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

    //set the pulses per revolutions of the encoder
    databuffer[0] = (uint8_t)(0x000000ff & enc.pulses_per_rev);
    databuffer[1] = (uint8_t)(0x000000ff & (enc.pulses_per_rev>>8));
    databuffer[2] = (uint8_t)(0x000000ff & (enc.pulses_per_rev>>16));
    databuffer[3] = (uint8_t)(0x000000ff & (enc.pulses_per_rev>>24));
	status = epos_WriteObject(handle, 0x3010, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

    //set encoder type

    databuffer[0] = enc.ind | (enc.direction << 4);
	status = epos_WriteObject(handle, 0x3010, 0x02, databuffer, 1);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_SetHallPolarity(UART_HandleTypeDef *handle, uint8_t polarity){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	 databuffer[0] = polarity;
	 status = epos_WriteObject(handle, 0x301A, 0x01, databuffer, 1);
	 if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_SetOperationMode(UART_HandleTypeDef *handle, int8_t mode){
	HAL_StatusTypeDef status;
	  uint8_t databuffer = (uint8_t) mode;
	 status = epos_WriteObject(handle, 0x6060, 0x00, &databuffer, 1);
	 if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_SetMaxSpeed(UART_HandleTypeDef *handle, MData motor, uint16_t type){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	uint32_t speed = motor.maxSpeed;

	switch(type){
		case(DC_MOTOR):
			if(speed > 100000) speed = 100000;
	    break;

		case(EC_BLOCK):
			if(speed > 100000/motor.numPolePairs) speed = 100000/motor.numPolePairs;
		break;

		case(EC_SINUS):
			if(speed > 50000/motor.numPolePairs) speed = 50000/motor.numPolePairs;
		break;

		default:
			speed = motor.maxSpeed;
	}

	databuffer[0] = (uint8_t)(0x000000ff & speed);
	databuffer[1] = (uint8_t)(0x000000ff & (speed>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (speed>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (speed>>24));

	 status = epos_WriteObject(handle, 0x6080, 0x00, databuffer, 4);
	 if(status != HAL_OK) return status;

	return status;
}






/* Set gains for current controller
 * P and I gain both have to be given in [uV/A]
 */
HAL_StatusTypeDef epos_SetCurCtrlGains(UART_HandleTypeDef *handle, CurrCtrl gains){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	databuffer[0] = (uint8_t)(0x000000ff & gains.P_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.P_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.P_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.P_gain>>24));

	 status = epos_WriteObject(handle, 0x30A0, 0x01, databuffer, 4);
	 if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.I_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.I_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.I_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.I_gain>>24));

	status = epos_WriteObject(handle, 0x30A0, 0x02, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

/* Set gains for position controller
 * P [uA/rad]
 * I [uA/(rad*s)]
 * D [(uA*s)/rad]
 * FF_velo [(uA*s)/rad]
 * FF_accel [(uA*s*s)/rad]
 */
HAL_StatusTypeDef epos_SetPosCtrlGains(UART_HandleTypeDef *handle, PosCtrl gains){
	HAL_StatusTypeDef status;

	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	databuffer[0] = (uint8_t)(0x000000ff & gains.P_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.P_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.P_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.P_gain>>24));

	 status = epos_WriteObject(handle, 0x30A1, 0x01, databuffer, 4);
	 if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.I_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.I_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.I_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.I_gain>>24));

	status = epos_WriteObject(handle, 0x30A1, 0x02, databuffer, 4);
	if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.D_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.D_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.D_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.D_gain>>24));

	status = epos_WriteObject(handle, 0x30A1, 0x03, databuffer, 4);
	if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.FF_velo_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>24));

	status = epos_WriteObject(handle, 0x30A1, 0x04, databuffer, 4);
	if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.FF_accel_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>24));

	status = epos_WriteObject(handle, 0x30A1, 0x05, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

/* Set gains for velocity controller
 * P [(uA*s)/rad]
 * I [uA/rad]
 * FF_velo [(uA*s)/rad]
 * FF_accel [(uA*s*s)/rad]
 */
HAL_StatusTypeDef epos_SetVeloCtrlGains(UART_HandleTypeDef *handle, VeloCtrl gains){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};

	databuffer[0] = (uint8_t)(0x000000ff & gains.P_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.P_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.P_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.P_gain>>24));

	 status = epos_WriteObject(handle, 0x30A2, 0x01, databuffer, 4);
	 if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.I_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.I_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.I_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.I_gain>>24));

	status = epos_WriteObject(handle, 0x30A2, 0x02, databuffer, 4);
	if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.FF_velo_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.FF_velo_gain>>24));

	status = epos_WriteObject(handle, 0x30A2, 0x03, databuffer, 4);
	if(status != HAL_OK) return status;

	databuffer[0] = (uint8_t)(0x000000ff & gains.FF_accel_gain);
	databuffer[1] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (gains.FF_accel_gain>>24));

	status = epos_WriteObject(handle, 0x30A2, 0x04, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}


//Set Torque offset in [Nm]
HAL_StatusTypeDef epos_SetTorqueOffset(UART_HandleTypeDef *handle, double offset, double divisor){
	HAL_StatusTypeDef status;
    uint8_t databuffer[2] = {0x00, 0x00};
    double tq_helper = 0.0;
    int16_t tq = 0x0000;

    tq_helper = offset / divisor;
    tq = ROUND_2_INT16(tq_helper);

    databuffer[0] = (uint8_t)(0x00ff & tq);
    databuffer[1] = (uint8_t)(tq>>8);
	status = epos_WriteObject(handle, 0x60B2, 0x00, databuffer, 2);
	if(status != HAL_OK) return status;

	return status;
}

//Set Position offset in [rad]
//It is assumed that the position unit is in [increments]
HAL_StatusTypeDef epos_SetPosOffset(UART_HandleTypeDef *handle, double offset, double numIncr){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	double offset_helper = 0.0;
	int32_t off = 0;

	offset_helper = offset * numIncr / (2 * PI);
	off = ROUND_2_INT32(offset_helper);

	databuffer[0] = (uint8_t)(0x000000ff & off);
	databuffer[1] = (uint8_t)(0x000000ff & (off>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (off>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (off>>24));

	status = epos_WriteObject(handle, 0x60B0, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

//Set Velocity offset in [rad/s]
//It is assumed that the velocity unit is in [increments/s]
HAL_StatusTypeDef epos_SetVeloOffset(UART_HandleTypeDef *handle, double offset, double numIncr){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	double offset_helper = 0.0;
	int32_t off = 0;

	offset_helper = offset * numIncr / (2 * PI);
	off = ROUND_2_INT32(offset_helper);

	databuffer[0] = (uint8_t)(0x000000ff & off);
	databuffer[1] = (uint8_t)(0x000000ff & (off>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (off>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (off>>24));

	status = epos_WriteObject(handle, 0x60B1, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}



/*Set Software position limit 
 * Max and Min have to be given in radians!
 * IMPORTANT: It is assumed that the position unit is set to "increments" in SI Unit Position (0x60A8)!
 * If this is not the case the function will not set the correct values!
 */
HAL_StatusTypeDef epos_SetPosLimit(UART_HandleTypeDef *handle, double min, double max, double numIncr){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	double lim_helper = 0.0;
	int32_t limit = 0;

	lim_helper = max * numIncr / (2 * PI);
	limit = ROUND_2_INT32(lim_helper);

	databuffer[0] = (uint8_t)(0x000000ff & limit);
	databuffer[1] = (uint8_t)(0x000000ff & (limit>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (limit>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (limit>>24));

	 status = epos_WriteObject(handle, 0x607D, 0x02, databuffer, 4);
	 if(status != HAL_OK) return status;

	lim_helper = min * numIncr / (2 * PI);
	limit = ROUND_2_INT32(lim_helper);

	databuffer[0] = (uint8_t)(0x000000ff & limit);
	databuffer[1] = (uint8_t)(0x000000ff & (limit>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (limit>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (limit>>24));

	status = epos_WriteObject(handle, 0x607D, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_StoreParameters(UART_HandleTypeDef *handle){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x65, 0x76, 0x61, 0x73};
	status = epos_WriteObject(handle, 0x1010, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_RestoreParameters(UART_HandleTypeDef *handle){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x64, 0x61, 0x6F, 0x6C};
	status = epos_WriteObject(handle, 0x1011, 0x01, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

HAL_StatusTypeDef epos_GetStatusword(UART_HandleTypeDef *handle, uint16_t *statusword){
	HAL_StatusTypeDef status;
    uint32_t databuffer;
    status = epos_ReadObject(handle, 0x6041, 0x00, &databuffer);
    if(status != HAL_OK) return status;

    *statusword = (uint16_t)(0x0000ffff & databuffer);

	return status;
}

/* Function to trigger State Transitions of the EPOS device */
HAL_StatusTypeDef epos_DeviceCommand(UART_HandleTypeDef *handle, uint8_t command){

	HAL_StatusTypeDef status;
	uint32_t databuffer_receive;
	uint8_t databuffer_send[4];
	uint8_t LSB; //Least significant byte of the controlword

	status = epos_ReadObject(handle, 0x6040, 0x00, &databuffer_receive); //Read in Control Word
	if(status != HAL_OK) return status;

	LSB = (uint8_t)(0x000000ff & databuffer_receive);

	switch(command){
		case SHUTDOWN:
				LSB |= 0b00000110;
				LSB &= 0b01111110;
				break;

		case SWITCH_ON:
				LSB |= 0b00000111;
				LSB &= 0b01111111;
				break;

		case SWITCH_ON_ENABLE_OP:
				LSB |= 0b00001111;
				LSB &= 0b01111111;
				break;

		case DISABLE_VOLT:
				LSB &= 0b01111101;
				break;

		case QUICK_STOP:
				LSB |= 0b00000010;
				LSB &= 0b01111011;
				break;

		case DISABLE_OP:
				LSB |= 0b00000111;
				LSB &= 0b01110111;
				break;

		case ENABLE_OP:
				LSB |= 0b00001111;
				LSB &= 0b01111111;
				break;

		case FAULT_RESET:
				LSB |= 0b10000000;
				break;

		//default: break;
	}

	databuffer_send[0] = LSB;
	databuffer_send[1] = (uint8_t)(0x000000ff & (databuffer_receive>>8));
	databuffer_send[2] = (uint8_t)(0x000000ff & (databuffer_receive>>16));
	databuffer_send[3] = (uint8_t)(0x000000ff & (databuffer_receive>>24));

	status = epos_WriteObject(handle, 0x6040, 0x00, databuffer_send, 4); //Write new Control Word
	if(status != HAL_OK) return status;


	return status;
}

//CONTROL related

/* SET TARGET TORQUE (Use only in Cyclic Synchronous Torque Mode!)
 * Set desired torque. The input argument "torque" is supposed to have the unit [Nm]!
 * The input argument divisor should be calculated in the Setup/Config function of the EPOS!
 * As mentioned in the Firmware Guide, the target torque value that is written to the Target
 * Torque Register/Object (0x6071) has to be in thousands of the motor rated torque. Therefore,
 * the Divisor can be calculated as follows: Divide the motor rated torque (which is stored in (0x6071)
 * and has the unit [uNm]) by (1000000*1000). The first factor in the divisor scales from [uNm]
 * to [Nm] and the second factor because we need thousands.
 *
 */
HAL_StatusTypeDef epos_SetDesiredTorque(UART_HandleTypeDef *handle, double torque, double divisor){
	HAL_StatusTypeDef status;
    uint8_t databuffer[2] = {0x00, 0x00};
    double tq_helper = 0.0;
    int16_t tq = 0x0000;

    tq_helper = torque / divisor;
    tq = ROUND_2_INT16(tq_helper);

    databuffer[0] = (uint8_t)(0x00ff & tq);
    databuffer[1] = (uint8_t)(tq>>8);
	status = epos_WriteObject(handle, 0x6071, 0x00, databuffer, 2);
	if(status != HAL_OK) return status;

	return status;
}

/*SET TARGET POSITION (Use only in Cyclic Synchronous Position Mode!)
 * The input pos has the unit [rad]
 * It is assumed that the position unit is in [increments]
 */
HAL_StatusTypeDef epos_SetDesiredPos(UART_HandleTypeDef *handle, double pos, double numIncr){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	double pos_helper = 0.0;
	int32_t position = 0x0000;

	pos_helper = pos * numIncr / (PI*2);
	position = ROUND_2_INT32(pos_helper);

	databuffer[0] = (uint8_t)(0x000000ff & position);
	databuffer[1] = (uint8_t)(0x000000ff & (position>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (position>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (position>>24));

	status = epos_WriteObject(handle, 0x607A, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}

/*SET TARGET VELOCITY (Use only in Cyclic Synchronous Velocity Mode!)
 * The input velo has the unit [rad/s]
 * It is assumed that the velocity unit is in [increments/s]
 */
HAL_StatusTypeDef epos_SetDesiredVelo(UART_HandleTypeDef *handle, double velo, double numIncr){
	HAL_StatusTypeDef status;
	uint8_t databuffer[4] = {0x00, 0x00, 0x00, 0x00};
	double velo_helper = 0.0;
	int32_t velocity = 0x0000;

	velo_helper = velo * numIncr / (PI*2);
	velocity = ROUND_2_INT32(velo_helper);

	databuffer[0] = (uint8_t)(0x000000ff & velocity);
	databuffer[1] = (uint8_t)(0x000000ff & (velocity>>8));
	databuffer[2] = (uint8_t)(0x000000ff & (velocity>>16));
	databuffer[3] = (uint8_t)(0x000000ff & (velocity>>24));

	status = epos_WriteObject(handle, 0x60FF, 0x00, databuffer, 4);
	if(status != HAL_OK) return status;

	return status;
}


/*
 * This functions saves the actual torque value on "torque". The unit is [Nm]. As
 * epos_SetDesiredTorque() it requires the divisor as input argument.
 */
HAL_StatusTypeDef epos_GetActualTorque(UART_HandleTypeDef *handle, double *torque, double divisor){
	HAL_StatusTypeDef status;
    uint32_t databuffer;
    int16_t castbuffer;
    status = epos_ReadObject(handle, 0x6077, 0x00, &databuffer);
    if(status != HAL_OK) return status;

    castbuffer = (int16_t)(0x0000ffff & databuffer);
    *torque = (double)castbuffer * divisor;

	return status;
}

/*
 * This functions saves the actual position value on "pos". The unit is [rad].
 * IMPORTANT: It is assumed that the position unit is set to "increments" in SI Unit Position (0x60A8)!
 * If this is not the case the function will output wrong data!
 *
 * Question: How is the reference position defined?
 */
HAL_StatusTypeDef epos_GetActualPos(UART_HandleTypeDef *handle, double *pos, double numIncr){
	HAL_StatusTypeDef status;
    uint32_t databuffer;
    int32_t castbuffer;
    status = epos_ReadObject(handle, 0x6077, 0x00, &databuffer);
    if(status != HAL_OK) return status;

    castbuffer = (int32_t)(databuffer);
    *pos = (double)castbuffer * 2 * (PI/numIncr);

	return status;
}

/*
 * This functions saves the actual speed value on "speed". The units is [rad/s].
 * IMPORTANT: It is assumed that the position unit is set to "increments per second" in SI Unit Velocity (0x60A9)!
 * If this is not the case the function will output wrong data!
 *
 */
HAL_StatusTypeDef epos_GetActualSpeed(UART_HandleTypeDef *handle, double *speed, double numIncr){
	HAL_StatusTypeDef status;
    uint32_t databuffer;
    int32_t castbuffer;
    status = epos_ReadObject(handle, 0x606C, 0x00, &databuffer);
    if(status != HAL_OK) return status;

    castbuffer = (int32_t)(databuffer);
    *speed = (double)castbuffer * 2 * (PI/numIncr);

	return status;
}

HAL_StatusTypeDef epos_Config(UART_HandleTypeDef *handle, EPOSMeta *meta){
	HAL_StatusTypeDef status = HAL_OK;

	status = epos_SetMotorType(handle, meta->type);
	if(status != HAL_OK) return status;

	status = epos_SetMotorData(handle, meta->data);
	if(status != HAL_OK) return status;

	status = epos_SetMotorAxis(handle, meta->axis);
	if(status != HAL_OK) return status;

	status = epos_ConfigEncoder(handle, meta->enc);
	if(status != HAL_OK) return status;

	status = epos_SetHallPolarity(handle, meta->hall_polarity);
	if(status != HAL_OK) return status;

	status = epos_SetUnits(handle, meta->units);
	if(status != HAL_OK) return status;

	status = epos_SetMaxSpeed(handle, meta->data, meta->type);
	if(status != HAL_OK) return status;

	status = epos_SetPosLimit(handle, meta->min_pos_lim, meta->max_pos_lim, (double)meta->enc.pulses_per_rev);
	if(status != HAL_OK) return status;

	status = epos_GetMotorRatedTorque(handle, &(meta->rated_torque));
	if(status != HAL_OK) return status;

	meta->divisor = (double)(meta->rated_torque);
	meta->divisor /= (1000000*1000);

	status = epos_SetOperationMode(handle, meta->op_mode);
	if(status != HAL_OK) return status;

	if(meta->op_mode == EPOS_CST){
		status = epos_SetTorqueOffset(handle, meta->torque_offset, meta->divisor);
		if(status != HAL_OK) return status;
	}
	else if (meta->op_mode == EPOS_CSV){
		//check if the velocity unit is [increments/s], if not skip setting the offset (the function is only suited to [increments/s] in its current implementation)
		if((meta->units.velo_pre == NOPREFIX) && (meta->units.velo_num == INCS) && (meta->units.velo_den == SECS)){
		status = epos_SetVeloOffset(handle, meta->velo_offset, (double)meta->enc.pulses_per_rev);
		if(status != HAL_OK) return status;
		}
	}
	else if (meta->op_mode == EPOS_CSP){

		status = epos_SetTorqueOffset(handle, meta->torque_offset, meta->divisor);
		if(status != HAL_OK) return status;

		//check if the position unit is [increments], if not skip setting the offset (the function is only suited to [increments] in its current implementation)
		if((meta->units.pos_pre == NOPREFIX) && (meta->units.pos_num == INCS) && (meta->units.pos_den == DIMLESS)){
		status = epos_SetPosOffset(handle, meta->pos_offset, (double)meta->enc.pulses_per_rev);
		if(status != HAL_OK) return status;
		}
	}

	return status;
}

