# EPOS4serial

Driver for the Maxon EPOS 4 positioning controller using serial communication. 

The driver has not been tested yet on the actual EPOS 4 positioning controller. However, the logic of all functions were tested and verified.

The driver was orginally developed to be used with an UART peripheral of a STM32 microcontroller. Therefore, it is necessary to use an external UART to RS232 converter if the used embedded system does not provide a RS232. Moreover, this is why the STM32 HAL library is used for the actual serial communication (HAL_UART_Receive(), HAL_UART_Transmit()) in the low level functions epos_ReadObject() and epos_WriteObject(). Also, this means that a HAL UART handle is required as input argument of each function.

The driver can be easily used with microcontrollers other than STM32. This requires to replace the HAL_UART_Receive() and HAL_UART_Transmit() functions and adjust the input arguments.

The NODE_ID has been defined to 0x01 in the DEFINITIONS section of epos4_serial header file. If necessary, please change the NODE_ID in the header file. 

Usage of the EPOS4serial driver within main.c:

If a stm32 is used:

- Initialize the UART peripheral and define an UART handle

- Define an EPOSMeta struct with the motor and sensor characteristics of the drive as well as units, etc.

- Call epos_Config()

- Call epos_DeviceCommand() in order to trigger a State Transition of the EPOS4

- When in "Operation Enabled", functions like epos_SetDesiredTorque() or epos_GetActualTorque() can be called 
